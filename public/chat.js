import { Peer } from "https://esm.sh/peerjs@1.5.3?bundle-deps"

var lastPeerId = null;
var peer = null; // Own peer object


const log = (message) => {
  console.log(message)
}

/**
 * Create the Peer object for our end of the connection.
 *
 * Sets up callbacks that handle any events related to our
 * peer object.
 */


const connections = {};

export const broadcast = (data) => {
  for (let conn in connections) {
    console.log(connections[conn].peer + ": " + connections[conn].open)

    connections[conn].send(data);
  }
}

export const initialize = (callback, dataCallback) => {

  // Create own peer object with connection to shared PeerJS server
  peer = new Peer(null);

  peer.on('open', function (id) {
    // Workaround for peer.reconnect deleting previous id
    if (peer.id === null) {
      log('Received null id from peer open');
      peer.id = lastPeerId;
    } else {
      lastPeerId = peer.id;
    }

    log('ID: ' + peer.id);
    callback(peer);

  });



  peer.on('disconnected', function () {

    log('Connection lost. Please reconnect');

    // Workaround for peer.reconnect deleting previous id
    peer.id = lastPeerId;
    peer._lastServerId = lastPeerId;
    peer.reconnect();
  });

  peer.on('connection', function (conn) {
    log("Connected to: " + conn.peer);
    connections[conn.peer] = conn;

    conn.on('data', (data) => {
      dataCallback(data, connections)
    });

  });
  peer.on('close', function () {
    for (let conn in connections) {
      conn = null;
    }
    log('Connection destroyed');
  });


  peer.on('error', function (err) {
    log(err);
  });

};

export const connect = (id, callback) => {
  var conn = peer.connect(id);
  log('Connecting to ' + id)
  conn.on('open',  () => {
    // Receive messages
    callback(conn)
  });
}