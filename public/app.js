import { initialize, connect, broadcast } from './chat.js'

const urlParams = new URLSearchParams(window.location.search);

const chatID = urlParams.get('chat');
var lastChat = localStorage.getItem('last-chat');
var lastChatJSON = [];
var server;
var sessionPeer;

var myAlias = {}
const aliases = localStorage.getItem('aliases') ? JSON.parse(localStorage.getItem('aliases')) : {};

if (lastChat)
  lastChatJSON = JSON.parse(lastChat);



document.addEventListener('DOMContentLoaded', () => {

  const chatBody = document.getElementById('chat-body');
  const chatInput = document.getElementById('chat-input');
  const sendButton = document.getElementById('send-button');

  sendButton.addEventListener('click', () => {
    const message = chatInput.value;
    if (message) {
      sendMessage(message)
    }
  });

  chatInput.addEventListener('keydown', (event) => {
    if (event.key === 'Enter' && !event.shiftKey) {
      const message = chatInput.value;
      if (message) {
        sendMessage(message)
      }
    }
  });


  function updateChatJSON(newChatMessage) {
    lastChatJSON.push(newChatMessage);
    localStorage.setItem('last-chat', JSON.stringify(lastChatJSON))
  }

  function sendMessage(message) {
    message = message.trim();

    const newChatMessage = {
      user: myAlias.name ? myAlias.name : 'owner',
      content: message,
      avatar: myAlias.avatar ? myAlias.avatar : './avatar/avatar-01.png'
    }

    if (server) {
      server.send(newChatMessage);
    } else {
      appendMessage(newChatMessage.user, newChatMessage.content, newChatMessage.avatar);
      updateChatJSON(newChatMessage);
      broadcast(newChatMessage);
    }

    chatInput.value = '';
  }

  function appendMessage(user, message, avatar) {
    const messageContainer = document.createElement('div');
    messageContainer.classList.add('chat-message');
    messageContainer.classList.add(user);
    messageContainer.classList.add(user == myAlias.name || !myAlias.name && user == 'owner' ? 'me' : 'others');
    messageContainer.innerHTML = `<img src="${avatar}"><p>${message}</p>`;
    chatBody.appendChild(messageContainer);
    chatBody.scrollTop = chatBody.scrollHeight;
    return messageContainer;
  }

  function renderChat(chat) {
    for (let message of chat) {
      appendMessage(message.user, message.content, message.avatar);
    }
  }

  const onDataFromServer = (data) => {
    if (data.alias != null) {
      myAlias = data.alias;
      localStorage.setItem('chat-uid', myAlias.secret);
      const lastChatJSONTemp = JSON.parse(data.history);
      renderChat(lastChatJSONTemp);
    } else {
      appendMessage(data.user, data.content, data.avatar);
    }
  }

  const onChatReady = (peer) => {
    renderChat(lastChatJSON);
    appendMessage('system', `<a href="?chat=${peer.id}" target="_blank">${peer.id}</a>`, './avatar/system.png');
  }


  const onConnect = (peer) => {

    sessionPeer = peer;
    if (chatID) {
      connect(chatID, (conn) => {
        server = conn;
        conn.on('data', function (data) {
          //console.log("DATA RECEIVED ON CLIENT", data)
          onDataFromServer(data)

        });

        var secret = localStorage.getItem('chat-uid');

        server.send({
          user: peer.id,
          secret: secret
        })

        window.addEventListener('beforeunload', function () {
          server.close();
          peer.disconnect();
        });

      });
    } else {

      onChatReady(peer)
    }



  }

  const onDataToServer = (data, connections) => {

    if (!data.content) {
      function generateId(user) {

        const newAlias = {
          name: "alias-" + Math.random().toString(36).substring(2, 9),
          secret: Math.random().toString(36).substring(2, 9),
          user: user,
          avatar: `./avatar/avatar-0${Math.floor(Math.random() * 10)}.png`
        };

        aliases[newAlias.secret] = newAlias;

        return newAlias;
      }

      if (data.secret && aliases[data.secret]) {

        aliases[data.secret].user = data.user; //update peer id

        connections[data.user].send({
          user: sessionPeer.id,
          alias: aliases[data.secret],
          history: JSON.stringify(lastChatJSON)
        });
      } else { // new user
        connections[data.user].send({
          user: sessionPeer.id,
          alias: generateId(data.user),
          history: JSON.stringify(lastChatJSON)
        })
      }

      localStorage.setItem('aliases', JSON.stringify(aliases));




    } else {
      broadcast(data);
      appendMessage(data.user, data.content, data.avatar);
      const newChatMessage = {
        user: data.user,
        content: data.content,
        avatar: data.avatar
      }
      updateChatJSON(newChatMessage);
    }


  };


  initialize(onConnect, onDataToServer);


})